export default colors = {
    yellow:'#FCEC0C',
    limeGreen:'#37B547',
    blue:'#4359A4',
    white:'#FAFBFA',
    inactive:'#EEE'
}