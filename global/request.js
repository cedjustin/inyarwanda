import config from './config';

// fetch all categories
export const allCategories = async () => {
    let data;
    let response = await fetch(config.rootUrl + 'get-categories', {
        headers: {
            appId: config.appId,
            appKey: config.appKey
        }
    });
    data = await response.json();
    data.categories.unshift({ name: 'Inkuru zose' })
    data.categories.forEach(element => {
        element.title = element.name
    });
    return data.categories;
}

export const headlineNews = async () => {
    var d = new Date();
    d.setDate(d.getDate() - 2);
    day = d.getDay();
    month = d.getMonth();
    year = d.getFullYear();
    date = year + '-' + month + '-' + day
    let response = await fetch(config.rootUrl+'get-posts?lastPubDate=' + date + '&type=recent&offset=10&limit=0&total=10&lang=kin-Kin', {
        headers: {
            appId: config.appId,
            appKey: config.appKey
        }
    });
    return data = response.json();
}

export const entertainment = async () => {
    let response = await fetch(config.rootUrl+'get-posts-by-category/72?offset=0&limit=5&total=89575&lang=kin-Kin', {
        headers: {
            appId: config.appId,
            appKey: config.appKey
        }
    });
    return data = response.json();
}

export const utuntuNutundi = async () => {
    let response = await fetch(config.rootUrl+'get-posts-by-category/22?offset=0&limit=5&total=89575&lang=kin-Kin', {
        headers: {
            appId: config.appId,
            appKey: config.appKey
        }
    });
    return data = response.json();
}