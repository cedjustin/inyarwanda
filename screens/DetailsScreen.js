import React from 'react';
import { ScrollView, StyleSheet, Dimensions, View } from 'react-native';
import Config from '../global/config';
import HTML from 'react-native-render-html';
import { ActivityIndicator } from '@ant-design/react-native';

export default class DetailsScreen extends React.Component {
    static navigationOptions = {
        headerTransparent: true,
        headerStyle: {
            backgroundColor: 'transparent'
        },
    };

    constructor(props) {
        super(props);
        this.state = ({
            rootUrl: Config.rootUrl,
            appId: Config.appId,
            appKey: Config.appKey,
            data: this.props.navigation.state.params,
            html: null,
            loader: true
        })
    }

    componentDidMount() {
        this.setState({
            html: data.content,
            loader: false
        })
    }


    render() {
        const htmlStyle = {
        }
        if (this.state.loader == true) {
            return (
                <View style={[styles.container, { justifyContent: 'center', alignItems: 'center' }]}>
                    <ActivityIndicator size='large' color={colors.blue} />
                </View>
            )
        } else {
            return (
                <ScrollView style={styles.container}>
                    <View style={{ margin: 20 }}>
                        <HTML html={this.state.data.content} imagesMaxWidth={Dimensions.get('window').width} tagsStyles={htmlStyle } />
                    </View>
                </ScrollView>
            );
        }
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 15,
        backgroundColor: '#fff',
    },
});
