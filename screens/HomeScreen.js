import React from 'react';
import {
  StyleSheet,
  View,
  ScrollView,
  Image,
  AsyncStorage,
  Dimensions,
  FlatList,
  RefreshControl
} from 'react-native';
import { TouchableRipple, Title, Card, Text, Divider, Avatar, Button } from 'react-native-paper';
import { ActivityIndicator, Carousel } from '@ant-design/react-native';
import moment from "moment";
// animation
import posed from 'react-native-pose';
// import redux
import { connect } from 'react-redux';
// import request page
import { allCategories, headlineNews, entertainment, utuntuNutundi } from '../global/request';
// importing constants
import colors from '../global/colors';

let DrawerMenu = (props) => {
  return (
    <View>
      <TouchableRipple style={{ width: Dimensions.get('window').width / 5, justifyContent: 'center', alignItems: 'center' }} onPress={() => props.navigation.openDrawer()}>
        <Image source={{ uri: 'https://img.icons8.com/windows/32/000000/align-right.png' }} style={{ width: '70%', height: '70%', margin: 2 }} resizeMode='contain' />
      </TouchableRipple>
    </View>
  )
}

let NotificationMenu = (props) => {
  return (
    <View>
      <TouchableRipple style={{ width: Dimensions.get('window').width / 5, justifyContent: 'center', alignItems: 'center' }}>
        <Image source={{ uri: 'https://img.icons8.com/carbon-copy/100/000000/bell.png' }} style={{ width: '70%', height: '70%', margin: 2 }} resizeMode='contain' />
      </TouchableRipple>
    </View>
  )
}

class HomeScreen extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    headerTitle: 'Inyarwanda',
    HeaderLeft: (<DrawerMenu navigation={navigation} />),
    headerRight: (<NotificationMenu navigation={navigation} />),
    headerStyle: {
      elevation: 0
    },
  });

  state = {
    loading: true,
    categories: [],
    headlineNews: [],
    entertainment: [],
    utuntuNutundi: [],
    refreshing: false,
    visible: 'closed'
  }

  componentDidMount = async () => {
    this._fetchData();
  }

  _getAllCategories = async () => {
    try {
      var data = await allCategories();
      await AsyncStorage.setItem('AllCategories', JSON.stringify(data));
    } catch (e) {
      alert(e);
    }
  }

  _getHeadlineNews = async () => {
    try {
      var data = await headlineNews();
      await AsyncStorage.setItem('HeadLineNews', JSON.stringify(data));
    } catch (e) {
      alert(e);
    }
  }

  _getEntertainment = async () => {
    try {
      var data = await entertainment();
      await AsyncStorage.setItem('Entertainment', JSON.stringify(data));
    } catch (e) {
      alert(e)
    }
  }

  _getUtuntu = async () => {
    try {
      var data = await utuntuNutundi();
      await AsyncStorage.setItem('Utuntu', JSON.stringify(data));
    } catch (e) {
      alert(e)
    }
  }

  // fetch online data
  _fetchOnlineData = async () => {
    await this._getAllCategories();
    await this._getEntertainment();
    await this._getHeadlineNews();
    await this._getUtuntu();
    this._fetchOfflineData();
  }

  _fetchOfflineData = async () => {
    try {
      var getAllCategories = await AsyncStorage.getItem('AllCategories');
      var getHeadlineNews = await AsyncStorage.getItem('HeadLineNews');
      var getEntertainment = await AsyncStorage.getItem('Entertainment');
      var getUtuntu = await AsyncStorage.getItem('Utuntu');
      var allCategoriesJson = JSON.parse(getAllCategories);
      var allHeadlineJson = JSON.parse(getHeadlineNews);
      var allEntertainmentJson = JSON.parse(getEntertainment);
      var allUtuntuJson = JSON.parse(getUtuntu);
      this.setState({
        categories: allCategoriesJson.posts
      });
      this.setState({
        headlineNews: allHeadlineJson.posts
      });
      this.setState({
        entertainment: allEntertainmentJson.posts
      });
      this.setState({
        utuntuNutundi: allUtuntuJson.posts
      });
    } catch (e) {
      alert(e);
    }
  }


  // fetch all data
  _fetchData = async () => {
    const status = await AsyncStorage.getItem('FirstTime');
    if (status == null) {
      await this._fetchOnlineData();
      await AsyncStorage.setItem('FirstTime', 'not the first time');
      this.setState({
        loading: false
      })
    } else {
      await this._fetchOfflineData();
      this.setState({
        loading: false
      });
      await this._fetchOnlineData();
    }
  }

  // changing date to moment
  _moment(date) {
    return time = moment(date).calendar();
  }

  _navigateToDetails(data) {
    this.props.navigation.navigate('Details', data);
  }

  render() {
    const posts = this.state.headlineNews.map(data => {
      return (
        <View style={styles.topCard}>
          <TouchableRipple onPress={() => alert(JSON.stringify(data))}>
            <Card style={styles.cCard}>
              <Card.Cover source={{ uri: data.logo }} />
              <Card.Content>
                <Title>{data.title}</Title>
              </Card.Content>
            </Card>
          </TouchableRipple>
        </View>
      )
    })
    const Imyidagaduro = () => {
      return (
        <View style={{ marginRight: 20 }}>
          <FlatList
            data={this.state.entertainment}
            keyExtractor={({ item, index }) => 'key'}
            renderItem={(item) => (
              <TouchableRipple onPress={() => alert(JSON.stringify(item))} style={{ marginTop: 5, marginBottom: 5 }}>
                <View>
                  <Divider style={{ margin: 10, height: 1, backgroundColor: 'gray' }} />
                  <View style={{ marginTop: 5, flexDirection: 'row' }}>
                    <View style={{ marginLeft: 5, marginRight: 5, flex: 1, flexWrap: 'wrap' }}>
                      <Title style={{ fontWeight: '700' }}>{item.item.title}</Title>
                      <View style={{ flexDirection: 'row', justifyContent: 'space-around' }}>
                        <Avatar.Image size={20} source={{ uri: item.item.author.picture }} style={{ backgroundColor: colors.limeGreen }} />
                        <Text style={{ color: 'gray' }}>{this._moment(item.item.published_at)}</Text>
                      </View>
                    </View>
                    <View>
                      <Image source={{ uri: item.item.logo }} style={{ width: Dimensions.get('window').width / 4, height: Dimensions.get('window').height / 8, borderRadius: 10 }} imageStyle={{ width: '100%', height: '100%' }} />
                    </View>
                  </View>
                </View>
              </TouchableRipple>
            )}
          />
        </View>
      )
    }
    const UtuntuNutundi = () => {
      return (
        <View>
          <FlatList
            data={this.state.utuntuNutundi}
            keyExtractor={({ item, index }) => 'key'}
            renderItem={(item) => (
              <TouchableRipple onPress={() => alert(JSON.stringify(item))} style={{ marginTop: 5, marginBottom: 5 }}>
                <View>
                  <View style={{ marginTop: 5 }}>
                    <View>
                      <Image source={{ uri: item.item.logo }} style={{ height: Dimensions.get('window').height / 3 }} imageStyle={{ width: '100%', height: '100%' }} />
                    </View>
                    <View style={{ marginLeft: 5, marginRight: 5, flex: 1, flexWrap: 'wrap' }}>
                      <Title style={{ fontWeight: '700' }}>{item.item.title}</Title>
                      <View style={{ flexDirection: 'row', justifyContent: 'space-around', marginTop: 5 }}>
                        <View style={{ flexDirection: 'row' }}>
                          <Avatar.Image size={20} source={{ uri: item.item.author.picture }} style={{ backgroundColor: colors.limeGreen }} />
                          <Text style={{ marginLeft: 5 }}>{item.item.author.full_name}</Text>
                        </View>
                        <Text style={{ color: 'gray' }}>{this._moment(item.item.published_at)}</Text>
                      </View>
                    </View>
                  </View>
                </View>
              </TouchableRipple>
            )}
          />
        </View>
      )
    }
    if (this.state.loading == true) {
      return (
        <View style={[styles.container, { justifyContent: 'center', alignItems: 'center' }]}>
          <ActivityIndicator size='large' color={colors.blue} />
        </View>
      )
    } else {
      return (
        <ScrollView
          style={styles.container}
        >
          <View style={{ margin: 20 }}>
            <Text style={styles.title}>Inkuru nyamukuru</Text>
          </View>
          <View style={{ marginLeft: 20, marginRight: 20 }}>
            <Carousel infinite autoplay style={{ marginBottom: 5 }} dotActiveStyle={{ backgroundColor: colors.blue }} dotStyle={{ backgroundColor: colors.inactive }}>
              {posts}
            </Carousel>
          </View>
          <View style={{ margin: 20 }}>
            <Text style={styles.title}>Imyidagaduro</Text>
            <View style={{ marginTop: 10 }}>
              <Imyidagaduro />
              <Button mode="outlined" style={{ borderColor: colors.blue, margin: 10 }} color={colors.blue}>izindi nkuru</Button>
            </View>
          </View>
          <View style={{ marginTop: 20, marginBottom: 20 }}>
            <View style={{margin:20}}>
              <Text style={styles.title}>Utunu nutundi</Text>
            </View>
            <View style={{ marginTop: 10 }}>
              <UtuntuNutundi />
              <Button mode="outlined" style={{ borderColor: colors.blue, margin: 10 }} color={colors.blue}>izindi nkuru</Button>
            </View>
          </View>
        </ScrollView>
      )
    }
  }
}

function mapStateToProps(state) {
  return {
    counter: state.counter
  }
}

function mapDispatchToProps(dispatch) {
  return {
    _increase: () => dispatch({ type: 'Increase' }),
    _decrease: () => dispatch({ type: 'Decrease' })
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white
  },
  topCard: {
    width: '100%',
    height: 'auto'
  },
  cCard: {
    elevation: 0,
    flex: 1,
    borderRadius: 15, margin: 5,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: colors.white
  },
  title: {
    color: colors.blue,
    fontSize: 20
  }
});
